/**
 * Created by Peter Hoang Nguyen on 9/14/2016.
 */
import { Injectable } from '@angular/core';
import { Hero } from '../entities/hero';
import { HEROES } from '../mocks/mock-heroes';

@Injectable()
export class HeroService {
    getHeroes(): Promise<Hero[]> {
        return Promise.resolve(HEROES);
    }
}